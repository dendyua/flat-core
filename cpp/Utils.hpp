
#pragma once

#include <cstdlib>
#include <type_traits>

#ifdef __linux__
#include <string.h>
#endif




namespace Flat {




#define FLAT_UNUSED(x) (void)x;

#ifdef __GNUC__
#	define FLAT_LIKELY(x)   __builtin_expect(!!(x), true)
#	define FLAT_UNLIKELY(x) __builtin_expect(!!(x), false)
#endif

#ifndef FLAT_LIKELY
#	define FLAT_LIKELY(x)   (x)
#	define FLAT_UNLIKELY(x) (x)
#endif




[[noreturn]] FLAT_CORE_EXPORT void abort(const char * format, ...);

#define __FLAT_PLAIN_ASSERT(condition, conditionString, file, line, function, format, ...) \
	if ( FLAT_UNLIKELY(!(condition)) ) { \
		Flat::abort(format, file, line, function, conditionString, ##__VA_ARGS__); \
	}

#define __FLAT_PLAIN_FATAL(file, line, function, format, ...) \
		Flat::abort(format, file, line, function, ##__VA_ARGS__);

#define FLAT_PLAIN_ASSERT(condition) \
	__FLAT_PLAIN_ASSERT(condition, #condition, __FILE__, __LINE__, __FUNCTION__, \
			"%s:%d %s - Assert failed (%s)")

#define FLAT_PLAIN_ASSERT_X(condition, ...) \
	__FLAT_PLAIN_ASSERT(condition, #condition, __FILE__, __LINE__, __FUNCTION__, \
			"%s:%d %s - Assert failed (%s): " __VA_ARGS__)

#define FLAT_PLAIN_FATAL \
	__FLAT_PLAIN_FATAL(__FILE__, __LINE__, __FUNCTION__, \
			"%s:%d %s - Fatal")

#define FLAT_PLAIN_FATAL_X(...) \
	__FLAT_PLAIN_FATAL(__FILE__, __LINE__, __FUNCTION__, \
			"%s:%d %s - Fatal: " __VA_ARGS__)




template <std::size_t N>
constexpr int stringLength(const char (&)[N])
{
	return N - 1;
}


constexpr int stringLength(const char * s, int i = 0)
{
	return s[i] == 0 ? i : stringLength(s, i+1);
}


template <typename String>
constexpr int stringCompare(String s1, String s2, int size, int i = 0)
{
	return
			i == size ? 0 :
			s1[i] < s2[i] ? -1 :
			s1[i] > s2[i] ? +1 :
			stringCompare(s1, s2, size, i + 1);
}


int strnlen(const char * str, std::size_t max);

#ifdef __linux__
inline int strnlen(const char * str, std::size_t max)
{
	return ::strnlen(str, max);
}
#endif


template <bool, typename E> struct _integral_resolver{ typedef E type; };
template <typename E> struct _integral_resolver<false, E>{ typedef std::underlying_type_t<E> type; };


template <typename T>
constexpr bool cleanBool(const T & value)
{ return value != static_cast<T>(false); }


template <typename T>
inline constexpr int _bitCounter(T v, int pos, int count) noexcept
{
	typedef typename _integral_resolver<std::is_integral_v<T>, T>::type I;
	typedef std::make_unsigned_t<I> U;

	if (pos == sizeof(T) * 8) return count;

	return _bitCounter(v, pos + 1, count + ((static_cast<U>(v) & (1 << pos)) ? 1 : 0));
}

template <typename T>
inline constexpr int bitCount(T v) noexcept
{
	return _bitCounter(v, 0, 0);
}

template <typename T>
inline constexpr int _bitPositioner(T v, int pos) noexcept
{
	typedef typename _integral_resolver<std::is_integral_v<T>, T>::type I;
	typedef std::make_unsigned_t<I> U;

	if (pos == sizeof(T) * 8) return -1;

	if (U(v) & (1 << pos)) return pos;

	return _bitPositioner(v, pos + 1);
}

template <typename T>
inline constexpr int bitPos(T v) noexcept
{
	return _bitPositioner(v, 0);
}




}
