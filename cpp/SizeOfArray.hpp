
#pragma once

#include <type_traits>




namespace Flat {




template <typename T>
constexpr int sizeOfArray(const T & t) noexcept;

template <typename T>
struct _SizeOfArray
{
	typedef std::remove_reference_t<T> RawType;
	static_assert(std::is_array_v<RawType>, "type must be array");
	inline static constexpr int value = std::extent_v<RawType>;
};

template <typename T>
inline constexpr int sizeOfArray_v = _SizeOfArray<T>::value;




template <typename T>
inline constexpr int sizeOfArray(const T &) noexcept
{
	typedef std::remove_reference_t<T> RawType;
	static_assert(std::is_array_v<RawType>, "value must be array");
	return std::extent_v<RawType>;
}




}
