
#pragma once

#include "Utils.hpp"

#include <functional>
#include <stdexcept>
#include <string>




namespace Flat {




template <typename String> class _StringRefInfo {};




template <typename String>
class StringRefBase
{
public:
	static const int kCharSize = sizeof(reinterpret_cast<String>(0)[0]);
	typedef typename _StringRefInfo<String>::Char Char;

	template <typename T> static constexpr T invalid_throw() { throw std::logic_error(""); }

	constexpr StringRefBase() noexcept;
	constexpr explicit StringRefBase(const std::string & string) noexcept;
	constexpr explicit StringRefBase(String string) noexcept;
	constexpr StringRefBase(String, int size) noexcept;
	constexpr StringRefBase(const StringRefBase<String> & other) noexcept;
	StringRefBase & operator=(const StringRefBase<String> & other) noexcept;

	constexpr bool operator==(const StringRefBase<String> & other) const noexcept;
	constexpr bool operator!=(const StringRefBase<String> & other) const noexcept;

	constexpr bool operator==(String other) const noexcept;
	constexpr bool operator!=(String other) const noexcept;

	constexpr bool operator<(const StringRefBase<String> & other) const noexcept;

	constexpr bool isNull() const noexcept;
	constexpr bool isEmpty() const noexcept;

	constexpr String string() const noexcept;
	constexpr int size() const noexcept;

	constexpr Char at(int index) const noexcept;
	constexpr Char operator[](int index) const noexcept;

	constexpr StringRefBase<String> chopBegin(int size) const noexcept;
	constexpr StringRefBase<String> chopEnd(int size) const noexcept;

	std::string toStdString() const noexcept;

	struct It {
		String s;
		int pos;

		Char operator*() const noexcept { return s[pos]; }
		bool operator==(const It & it) const noexcept { return pos == it.pos; }
		bool operator!=(const It & it) const noexcept { return pos != it.pos; }
		void operator++() noexcept { pos++; }
	};

	It begin() const noexcept { return It{string_, 0}; }
	It end() const noexcept { return It{string_, size_}; }

private:
	static constexpr String _validateString(String string);
	static constexpr int _validateSize(int size);

private:
	String string_;
	int size_;
};




template <> class _StringRefInfo<const char*> { public: typedef char Char; };
template <> class _StringRefInfo<char*> { public: typedef char Char; };




typedef StringRefBase<const char*> StringRef;
typedef StringRefBase<char*> MutableStringRef;




template <typename String>
constexpr String StringRefBase<String>::_validateString(const String string)
{
	return string ? string : invalid_throw<String>();
}

template <typename String>
constexpr int StringRefBase<String>::_validateSize(const int size)
{
	return size >= 0 ? size : invalid_throw<int>();
}

template <typename String>
constexpr StringRefBase<String>::StringRefBase() noexcept :
	string_(nullptr),
	size_(0)
{
}

template <typename String>
constexpr StringRefBase<String>::StringRefBase(const std::string & string) noexcept :
	string_(string.c_str()),
	size_(int(string.size()))
{
}

template <typename String>
constexpr StringRefBase<String>::StringRefBase(const String string, const int size) noexcept :
	string_(_validateString(string)),
	size_(_validateSize(size))
{
}

template <typename String>
constexpr StringRefBase<String>::StringRefBase(const String string) noexcept :
	string_(string),
	size_(string ? stringLength(string) : 0)
{
}

template <typename String>
constexpr StringRefBase<String>::StringRefBase(const StringRefBase<String> & other) noexcept :
	string_(other.string()),
	size_(other.size())
{
}

template <typename String>
inline StringRefBase<String> & StringRefBase<String>::operator=(
		const StringRefBase<String> & other) noexcept
{
	string_ = other.string();
	size_ = other.size();
	return *this;
}

template <typename String>
constexpr bool StringRefBase<String>::operator==(const StringRefBase<String> & other) const noexcept
{
	if (size() != other.size())  return false;
	if (string() == other.string()) return true;
	if (isNull() || other.isNull()) return false;
	return stringCompare(string(), other.string(), size()) == 0;
}

template <typename String>
constexpr bool StringRefBase<String>::operator!=(const StringRefBase<String> & other) const noexcept
{
	return !operator==(other);
}

template <typename String>
constexpr bool StringRefBase<String>::operator==(const String other) const noexcept
{
	return operator==(StringRefBase(other));
}

template <typename String>
constexpr bool StringRefBase<String>::operator!=(const String other) const noexcept
{
	return !operator==(other);
}

template <typename String>
constexpr bool StringRefBase<String>::operator<(const StringRefBase<String> & other) const noexcept
{
	return
			isNull() || other.isNull() ? false :
			size() != other.size() ? size() < other.size() :
			stringCompare(string(), other.string(), size()) < 0;
}

template <typename String>
constexpr bool StringRefBase<String>::isNull() const noexcept
{
	return string() == nullptr;
}

template <typename String>
constexpr bool StringRefBase<String>::isEmpty() const noexcept
{
	return size() == 0;
}

template <typename String>
constexpr String StringRefBase<String>::string() const noexcept
{
	return string_;
}

template <typename String>
constexpr int StringRefBase<String>::size() const noexcept
{
	return size_;
}

template <typename String>
constexpr typename StringRefBase<String>::Char StringRefBase<String>::at(
		const int index) const noexcept
{
	return index >= 0 && index < size() ? string_[index] :
			invalid_throw<typename StringRefBase<String>::Char>();
}

template <typename String>
constexpr typename StringRefBase<String>::Char StringRefBase<String>::operator[](
		const int index) const noexcept
{
	return string_[index];
}

template <typename String>
constexpr StringRefBase<String> StringRefBase<String>::chopBegin(const int size) const noexcept
{
	return size < 0 ? invalid_throw<StringRefBase<String>>() :
			size > size_ ? StringRefBase<String>() :
			StringRefBase<String>(string_ + size, size_ - size);
}

template <typename String>
constexpr StringRefBase<String> StringRefBase<String>::chopEnd(const int size) const noexcept
{
	return size < 0 ? invalid_throw<StringRefBase<String>>() :
			size > size_ ? StringRefBase<String>() :
			StringRefBase<String>(string_, size_ - size);
}


template <typename String>
std::string StringRefBase<String>::toStdString() const noexcept
{
	return isNull() ? std::string() : std::string(string_, size_);
}




extern FLAT_CORE_EXPORT size_t stringref_hash(const char * s, int size);




}




namespace std {

template <typename String>
struct FLAT_CORE_EXPORT hash<Flat::StringRefBase<String>> {
	inline size_t operator()(const Flat::StringRefBase<String> & s) const
	{ return Flat::stringref_hash(s.string(), s.size()); }
};

}
