
#include "StringRef.hpp"

#include <algorithm>
#include <cstring>




namespace Flat {




size_t stringref_hash(const char * const s, const int size)
{
	size_t h = 0;
	const auto func = std::hash<uint64_t>();
	for (int i = 0; i < size; i += sizeof(uint64_t)) {
		uint64_t tmp;
		const int len = std::min(size - i, int(sizeof(uint64_t)));
		const uint64_t & v = [&tmp, &s, i, len] () -> const uint64_t & {
			const char * const p = s + i;
			if (len < int(sizeof(uint64_t))) {
				tmp = 0;
				std::memcpy(&tmp, p, size_t(len));
				return tmp;
			} else {
				return reinterpret_cast<const uint64_t&>(*p);
			}
		}();
		h += func(v);
	}
	return h;
}




}
