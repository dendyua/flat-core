
#include "Utils.hpp"

#include <cstdarg>
#include <cstdio>




namespace Flat {




#ifdef __QNXNTO__
int strnlen(const char * str, std::size_t max)
{
	for ( int i = 0; i < static_cast<int>(max); ++i ) {
		if ( !str[i] ) {
			return i;
		}
	}
	return max;
}
#endif




void abort(const char * format, ...)
{
	va_list args;
	va_start(args, format);
	std::vfprintf(stderr, format, args);
	va_end(args);
	std::fprintf(stderr, "\n");
	std::abort();
}




}
