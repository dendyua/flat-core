
#pragma once

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#	define FLAT_OS_WIN
#else
#	define FLAT_OS_UNIX
#endif

#ifdef FLAT_OS_WIN
#	define FLAT_DECL_EXPORT __declspec(dllexport)
#	define FLAT_DECL_IMPORT __declspec(dllimport)
#else
#	define FLAT_DECL_EXPORT __attribute__((visibility("default")))
#	define FLAT_DECL_IMPORT __attribute__((visibility("default")))
#endif
