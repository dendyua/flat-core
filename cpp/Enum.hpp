
#pragma once

#include <type_traits>




namespace Flat {




template <typename E>
struct EnumTraits
{
	static inline constexpr E Null = static_cast<E>(0);
};




template <typename E>
struct Enum
{
	static_assert(std::is_enum_v<E>, "");

	typedef std::underlying_type_t<E> I;
	typedef std::make_unsigned_t<I> UI;

	static constexpr Enum fromInt(I i) noexcept { return Enum(i); }

	constexpr Enum(E value = EnumTraits<E>::Null) noexcept : value_(value) {}

	template <typename J = I> constexpr J asInt() const noexcept { return static_cast<J>(value_); }
	constexpr bool test(E flag) const noexcept { return (_int() & static_cast<UI>(flag)) != 0; }
	constexpr bool testAll(E flag) const noexcept { return (_int() & static_cast<UI>(flag)) == static_cast<UI>(flag); }
	template <typename J = I> constexpr J intMask(E m) const noexcept { return static_cast<J>(_int() & static_cast<UI>(m)); }
	constexpr Enum<E> mask(E m) const noexcept { return static_cast<E>(intMask<UI>(m)); }
	constexpr Enum<E> filter(E m) const noexcept { return mask(static_cast<E>(~static_cast<UI>(m))); }
	constexpr void set(E flag, bool on = true) noexcept { if (on) _int() |= static_cast<I>(flag); else clear(flag); }
	constexpr void add(I value) noexcept { _int() |= value; }
	constexpr void clear(E flag) noexcept { _int() &= ~static_cast<UI>(flag); }
	constexpr bool isEmpty() const noexcept { return _int() == 0; }

	constexpr operator E() const noexcept { return value_; }
	constexpr E value() const noexcept { return value_; }

	struct FlagsContainer {
		explicit FlagsContainer(const E value) noexcept : value(value) {}
		const E value;

		struct It {
			typedef typename Enum<E>::UI I;

			const Enum<E> value;
			int pos;

			void next() noexcept {
				while (true) {
					pos++;
					if (pos >= sizeof(E) * 8) break;
					const E flag = static_cast<E>(I(1) << pos);
					const bool set = value.test(flag);
					if (set) break;
				}
			}

			Enum<E> operator*() const noexcept {
				const E flag = static_cast<E>(I(1) << pos);
				const auto e = value.mask(flag);
				return e;
			}

			bool operator==(const It & it) const noexcept { return pos == it.pos; }
			bool operator!=(const It & it) const noexcept { return pos != it.pos; }
			void operator++() noexcept { next(); }
		};

		It begin() const { It it{value, -1}; it.next(); return it; }
		It end() const { return It{value, sizeof(E) * 8}; }
	};

	FlagsContainer flags() const noexcept {
		return FlagsContainer(value_);
	}

private:
	constexpr Enum(I i) noexcept : value_(static_cast<E>(i)) {}

	constexpr UI _int() const noexcept { return static_cast<UI>(value_); }
	constexpr UI & _int() noexcept { return reinterpret_cast<UI&>(value_); }

private:
	E value_;
};

#define FLAT_CORE_DEFINE_ENUM_OPS(E) \
	inline E operator|(E f1, E f2) noexcept \
	{ Flat::Enum<E> e(f1); e.set(f2); return e; }

#define FLAT_CORE_DEFINE_ENUM_NUMBER_OPS(E) \
	inline E operator|(E f, int number) noexcept \
	{ Flat::Enum<E> e(f); e.add(static_cast<Flat::Enum<E>::I>(number)); return e; }




}
