
#pragma once

#include <algorithm>
#include <cstdint>




namespace Flat {
namespace Endian {




#if defined(_MSC_VER)
#	if defined(__i386) || defined(__i386__) || defined(_M_IX86) || \
		defined(__x86_64) || defined(__x86_64__) || defined(__amd64) || defined(_M_X64)
static constexpr uint32_t kLittleEndian = 1234;
static constexpr uint32_t kBigEndian    = 4321;
static constexpr uint32_t kEndian       = kLittleEndian;
#	else
#		error "Unsuppored platform"
#	endif

#else
static constexpr uint32_t kLittleEndian = __ORDER_LITTLE_ENDIAN__;
static constexpr uint32_t kBigEndian    = __ORDER_BIG_ENDIAN__;
static constexpr uint32_t kEndian       = __BYTE_ORDER__;
#endif




template <typename T>
T fromBig(const void * data) noexcept;

template <typename T>
T fromLittle(const void * data) noexcept;




template <typename T>
inline void swapBytes(T & n) noexcept
{
	constexpr int size = sizeof(T);
	uint8_t * const d = reinterpret_cast<uint8_t*>(&n);
	for (int i = 0; i < size/2; ++i) std::swap(d[i], d[size - i - 1]);
}


template <typename T>
inline T fromBig(const void * const data) noexcept
{
	T v = reinterpret_cast<const T*>(data);
	if (kEndian != kBigEndian) swapBytes(v);
	return v;
}


template <typename T>
inline T fromLittle(const void * const data) noexcept
{
	T v = *reinterpret_cast<const T*>(data);
	if (kEndian != kLittleEndian) swapBytes(v);
	return v;
}




}}
