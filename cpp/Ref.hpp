
#pragma once




namespace Flat {




template <typename T>
class Ref
{
public:
	Ref(T & value) noexcept;
	Ref(const Ref &) = delete;
	~Ref() noexcept;

	const T * get() const noexcept;
	T * get() noexcept;

	Ref & operator=(const Ref &) = delete;

	const T & operator*() const noexcept;
	T & operator*() noexcept;
	const T * operator->() const noexcept;
	T * operator->() noexcept;

private:
	T & value_;
};




template <typename T>
inline Ref<T>::Ref(T & value) noexcept :
	value_(value)
{}

template <typename T>
inline Ref<T>::~Ref() noexcept
{ delete &value_; }

template <typename T>
inline const T * Ref<T>::get() const noexcept
{ return &value_; }

template <typename T>
inline T * Ref<T>::get() noexcept
{ return &value_; }

template <typename T>
const T & Ref<T>::operator*() const noexcept
{ return value_; }

template <typename T>
inline T & Ref<T>::operator*() noexcept
{ return value_; }

template <typename T>
inline const T * Ref<T>::operator->() const noexcept
{ return &value_; }

template <typename T>
inline T * Ref<T>::operator->() noexcept
{ return &value_; }




}
