
#pragma once

#include <cstdint>
#include <cstring>
#include <type_traits>

#include "StringRef.hpp"




namespace Flat {




template <typename E>
struct StringEnumItem
{
	const E value;
	const StringRef name;
};




template <typename E> struct StringEnumTraits;
template <typename E> struct StringEnumData;




template <typename E>
class StringEnumInfo
{
public:
	typedef StringEnumTraits<E> T;
	typedef StringEnumData<E> D;
	typedef std::underlying_type_t<E> I;

	inline static constexpr E last = static_cast<E>(static_cast<I>(T::max) - 1);
	inline static constexpr E null = static_cast<E>(static_cast<I>(T::first) - 1);

	inline static constexpr int firstBitPos = T::flags ? bitPos(T::first) : 0;
	inline static constexpr int lastBitPos = T::flags ? bitPos(last) : 0;

	static_assert(!T::flags || bitCount(T::first) == 1, "");
	static_assert(!T::flags || bitCount(last) == 1, "");

	static constexpr int valueToIndex(E value) noexcept
	{
		if (T::flags) {
			return bitPos(value) - firstBitPos;
		} else {
			return static_cast<I>(value) - static_cast<I>(T::first);
		}
	}

	static constexpr E valueFromIndex(I index) noexcept
	{
		if (T::flags) {
			return static_cast<E>(1 << (firstBitPos + index));
		} else {
			return static_cast<E>(static_cast<I>(T::first) + index);
		}
	}

	static constexpr int validate() noexcept {
		return T::flags ?
				_validateFlags() :
				_validateSequence();
	}

private:
	static constexpr bool _validateSequenceItem(int i);
	static constexpr bool _validateSequence();

	static constexpr bool _validateFlagsItem(int i);
	static constexpr bool _validateFlags();
};




template <typename E>
class StringEnum
{
public:
	typedef E Type;

	typedef StringEnumInfo<E> Info;

	static StringEnum<E> fromString(const StringRef & format, E defaultValue = Info::null) noexcept;
	static StringEnum<E> fromString(const char * format, E defaultValue = Info::null) noexcept;

	static StringEnum<E> fromBit(int pos) noexcept;

	inline static constexpr StringEnum<E> first() noexcept { return StringEnum<E>(Info::T::first); }
	inline static constexpr StringEnum<E> last() noexcept { return StringEnum<E>(Info::last); }

	constexpr StringRef toString() const noexcept;

	constexpr StringEnum() noexcept;
	constexpr StringEnum(Type value) noexcept;

	constexpr operator Type() const noexcept;
	constexpr Type value() const noexcept;

	constexpr bool isNull() const noexcept;

	// forbid implicit casting between enum and bool
	explicit operator bool() const = delete;

	constexpr bool operator==(const StringEnum<E> & other) const noexcept;
	constexpr bool operator!=(const StringEnum<E> & other) const noexcept;
	constexpr bool operator==(const Type & other) const noexcept;
	constexpr bool operator!=(const Type & other) const noexcept;

	constexpr bool operator<(const StringEnum<E> & other) const noexcept;

	// left side comparisons
	template <typename K> friend constexpr bool operator==(const typename StringEnum<K>::Type & a,
			const StringEnum<K> & b) noexcept;
	template <typename K> friend constexpr bool operator!=(const typename StringEnum<K>::Type & a,
			const StringEnum<K> & b) noexcept;

private:
	Type value_;
};




template <typename E>
constexpr bool StringEnumInfo<E>::_validateSequenceItem(const int i)
{
	if (i == D::count) return true;
	if (static_cast<E>(I(T::first) + I(i)) != D::items[i].value) throw "value mismatch";
	return _validateSequenceItem(i + 1);
}


template <typename E>
constexpr bool StringEnumInfo<E>::_validateSequence()
{
	if (I(T::max) - I(T::first) != D::count) throw "array size mismatch";
	return _validateSequenceItem(0);
}


template <typename E>
constexpr bool StringEnumInfo<E>::_validateFlagsItem(const int i)
{
	constexpr int count = bitPos(last) - firstBitPos + 1;
	if (i == count) return true;
	if (static_cast<E>(I(1) << (firstBitPos + i)) != D::items[i].value) throw "value mismatch";
	return _validateFlagsItem(i + 1);
}


template <typename E>
constexpr bool StringEnumInfo<E>::_validateFlags()
{
	if (lastBitPos - firstBitPos + 1 != D::count) throw "array size mismatch";
	return _validateFlagsItem(0);
}




template <typename E>
inline StringEnum<E> StringEnum<E>::fromString(const StringRef & format,
		const E defaultValue) noexcept
{
	if (format.isEmpty()) return StringEnum<E>(defaultValue);

	for (int i = 0; i < Info::D::count; ++i) {
		const StringEnumItem<E> & item = Info::D::items[i];
		if (!item.name) continue;
		if (format == item.name) return StringEnum<E>(item.value);
	}

	return StringEnum<E>(defaultValue);
}


template <typename E>
inline StringEnum<E> StringEnum<E>::fromString(const char * const format,
		const E defaultValue) noexcept
{
	if (!format || !*format) return StringEnum<E>(defaultValue);

	for (int i = 0; i < Info::D::count; ++i) {
		const StringEnumItem<E> & item = Info::D::items[i];
		if (item.name.isNull()) continue;
		if (item.name == format) return StringEnum<E>(item.value);
	}

	return StringEnum<E>(defaultValue);
}


template <typename E>
inline StringEnum<E> StringEnum<E>::fromBit(const int pos) noexcept
{
	typedef typename Info::I I;
	const E value = static_cast<E>(I(1) << pos);
	return StringEnum<E>(value);
}


template <typename E>
constexpr StringRef StringEnum<E>::toString() const noexcept
{
	if (isNull()) return StringRef();
	const int index = Info::valueToIndex(value());
	if (index < 0 || index >= Info::D::count) return StringRef();
	 return Info::D::items[index].name;
}


template <typename E>
constexpr StringEnum<E>::StringEnum() noexcept :
	value_(Info::null)
{
}


template <typename E>
constexpr StringEnum<E>::StringEnum(const Type value) noexcept :
	value_(value)
{
}


template <typename E>
constexpr StringEnum<E>::operator Type() const noexcept
{ return value_; }

template <typename E>
constexpr typename StringEnum<E>::Type StringEnum<E>::value() const noexcept
{ return value_; }

template <typename E>
constexpr bool StringEnum<E>::isNull() const noexcept
{ return value_ == Info::null; }

template <typename E>
constexpr bool StringEnum<E>::operator==(const StringEnum<E> & other) const noexcept
{ return value_ == other.value_; }

template <typename E>
constexpr bool StringEnum<E>::operator!=(const StringEnum<E> & other) const noexcept
{ return !operator==(other); }

template <typename E>
constexpr bool StringEnum<E>::operator==(const typename StringEnum<E>::Type & other) const noexcept
{ return operator==(StringEnum<E>(other)); }

template <typename E>
constexpr bool StringEnum<E>::operator!=(const typename StringEnum<E>::Type & other) const noexcept
{ return !operator==(other); }

template <typename E>
constexpr bool StringEnum<E>::operator<(const StringEnum<E> & other) const noexcept
{ return Info::valueToIndex(value_) < Info::valueToIndex(other.value_); }

template <typename E>
constexpr bool operator==(const typename StringEnum<E>::Type & a, const StringEnum<E> & b) noexcept
{ return b.operator==(a); }

template <typename E>
constexpr bool operator!=(const typename StringEnum<E>::Type & a, const StringEnum<E> & b) noexcept
{ return b.operator!=(a); }




}
