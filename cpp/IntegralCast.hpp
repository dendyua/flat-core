
#pragma once

#include <cstdint>




namespace Flat {




// 32/64-bit cast helpers between pointer and integer types.

// The cast functions throw error at compile time attempting to cast integer to pointer and vice
// versa when destination type size is lesser than source type size.
// Typical case is catch error when casting void* to int on 64-bit build.

// Examples:

// convert pointer to int:
//    void * p = ...;
//    int i = pointer_to_int_cast<int>(p); // breaks compillation on 64-bit build

// atomic pointer swap:
//    void * p1 = ...;
//    void * p2 = ...;
//    android_atomic_acquire_cas(0, pointer_to_int_cast<int>(p2), &pointer_to_int_cast_ref<int>(p1));

template <int PointerSize, int IntSize, typename T, typename I>
class _CasterBetweenPointerAndInteger
{
public:
	static T* int_to_pointer_cast(const I i);
	static I pointer_to_int_cast(const T* p);

	static T* & int_to_pointer_cast_ref(I & i);
	static I & pointer_to_int_cast_ref(T* & p);
};

template <int PointerSize, int IntSize, typename T, typename I>
inline T* _CasterBetweenPointerAndInteger<PointerSize, IntSize, T, I>::int_to_pointer_cast(const I i) {
	static_assert(IntSize <= PointerSize, "Invalid cast");
	return reinterpret_cast<T*>(i);
}

template <int PointerSize, int IntSize, typename T, typename I>
inline I _CasterBetweenPointerAndInteger<PointerSize, IntSize, T, I>::pointer_to_int_cast(const T* p) {
	static_assert(PointerSize <= IntSize, "Invalid cast");
	return reinterpret_cast<I>(p);
}

template <int PointerSize, int IntSize, typename T, typename I>
inline T* & _CasterBetweenPointerAndInteger<PointerSize, IntSize, T, I>::int_to_pointer_cast_ref(I & i) {
	static_assert(IntSize <= PointerSize, "Invalid cast");
	return *reinterpret_cast<T**>(&i);
}

template <int PointerSize, int IntSize, typename T, typename I>
inline I & _CasterBetweenPointerAndInteger<PointerSize, IntSize, T, I>::pointer_to_int_cast_ref(T* & p) {
	static_assert(PointerSize <= IntSize, "Invalid cast");
	return *reinterpret_cast<I*>(&p);
}




template <typename T, typename I>
inline T* int_to_pointer_cast(const I i) {
	return _CasterBetweenPointerAndInteger<sizeof(T*), sizeof(I), T, I>::int_to_pointer_cast(i);
}

template <typename I, typename T>
inline I pointer_to_int_cast(const T* p) {
	return _CasterBetweenPointerAndInteger<sizeof(T*), sizeof(I), T, I>::pointer_to_int_cast(p);
}

template <typename T, typename I>
inline T* & int_to_pointer_cast_ref(I & i) {
	return _CasterBetweenPointerAndInteger<sizeof(T*), sizeof(I), T, I>::int_to_pointer_cast_ref(i);
}

template <typename I, typename T>
inline I & pointer_to_int_cast_ref(T* & p) {
	return _CasterBetweenPointerAndInteger<sizeof(T*), sizeof(I), T, I>::pointer_to_int_cast_ref(p);
}




template <int Size = sizeof(void*)> class IntPointer;

template <>
class IntPointer<4>
{
public:
	typedef int32_t S;
	typedef uint32_t U;
};

template <>
class IntPointer<8>
{
public:
	typedef int64_t S;
	typedef uint64_t U;
};




}
