
cmake_minimum_required(VERSION 3.3)

find_package(Flat REQUIRED)

function(_flat_core_exec)
	get_filename_component(root_dir "${CMAKE_CURRENT_LIST_DIR}/.." ABSOLUTE)

	flat_add_sources(
		DIR "${root_dir}/cpp"
			StringRef.cpp
			Utils.cpp
	)

	add_library(Flat_Core ${flat_library_type} ${SOURCES})

	target_compile_features(Flat_Core PUBLIC cxx_std_17)

	target_include_directories(Flat_Core INTERFACE "${root_dir}/include")

	flat_add_library_export_macro(Flat_Core FLAT_CORE_EXPORT)

	if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU" OR CMAKE_CXX_COMPILER_ID STREQUAL "Clang"
			OR CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
		target_compile_options(Flat_Core PUBLIC
			-Werror=return-type
			-Werror=unused-result
			-Werror=implicit-fallthrough
		)
		if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
			target_compile_options(Flat_Core PUBLIC
				-Werror=implicit-exception-spec-mismatch
			)
		endif()
		if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU" AND WIN32)
			target_compile_definitions(Flat_Core PUBLIC
				_WIN32_WINNT=0x600
			)
		endif()
		if (CMAKE_COMPILER_IS_GNUCXX)
			target_compile_options(Flat_Core PUBLIC -Wno-terminate)
		endif()
	endif()

	if (MSVC)
		target_compile_definitions(Flat_Core PUBLIC _CRT_SECURE_NO_WARNINGS)
	endif()

	set_target_properties(Flat_Core PROPERTIES
		EXCLUDE_FROM_ALL YES
	)
endfunction()

_flat_core_exec()
